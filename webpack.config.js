// Libraries
const Path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: process.env.NODE_ENV || 'development',
    entry: Path.join(__dirname, 'src', 'index.js'),
    devServer: {
        contentBase: Path.join(__dirname, 'src'),
        historyApiFallback: true
    },
    output: {
        path: Path.join(__dirname, 'build'),
        filename: 'index.bundle.js',
        publicPath: '/'
    },
    resolve: {
        modules: [
            Path.resolve(__dirname, 'src'),
            'node_modules'
        ],
        alias: {
            Page: Path.resolve(__dirname, 'src', 'page'),
            Common: Path.resolve(__dirname, 'src', 'common'),
            Utility: Path.resolve(__dirname, 'src', 'utility')
        }
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                resolve: {
                    extensions: [ ".js", ".jsx" ]
                },
                use: [
                    "babel-loader"
                ]
            },
            {
                test: /\.(css|scss)$/,
                use: [
                    "style-loader",
                    "css-loader"
                ]
            },
            {
                test: /\.(jpg|jpeg|png|gif|mp3|svg)$/,
                use: [
                    "file-loader"
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: Path.join(__dirname, 'src', 'index.html')
        })
    ]
}
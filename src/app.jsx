// Libraries
import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'

// Segments
import Home from 'Page/home'
import Product from 'Page/product'

// Constants
const drawerSize = 260
const useStyles = makeStyles((theme) => ({
    wrapper: {
        width: '100%',
        height: '100%',
        display: 'flex'
    },
    container: {
        flexGrow: 1,
        marginLeft: -drawerSize,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    shifted: {
        marginLeft: 0,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    padder: {
        height: 64,
        [theme.breakpoints.down('md')]: {
            height: 56
        }
    },
    viewer: {
        height: `calc(100% - 64px)`,
        overflow: 'hidden auto'
    }
}))

export const App = () => {

    return (
        <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/:id' component={Product}/>
        </Switch>
    )

}
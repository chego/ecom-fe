// Libraries
import React from 'react'
import ReactDOM from 'react-dom'
import CssBaseline from '@material-ui/core/CssBaseline'
import { BrowserRouter as Router } from 'react-router-dom'
import 'regenerator-runtime/runtime.js'
import 'react-responsive-carousel/lib/styles/carousel.min.css'

// Modules
import { App } from './app'

// Styles
import 'index.css'

ReactDOM.render(
    <Router>
        <CssBaseline/>
        <App/>
    </Router>,
    document.getElementById('root')
)
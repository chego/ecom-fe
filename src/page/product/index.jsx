// Libraries
import React from 'react'
import Lodash from 'lodash'
import { useParams } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'

// Components
import Header from 'Page/product/header'
import Content from 'Page/product/content'

// Utilities
import { execute } from 'Utility/request'

const useStyles = makeStyles((theme) => ({
    wrapper: {
        padding: theme.spacing(3)
    }
}))

export default function Product() {

    const classes = useStyles()
    const { id } = useParams()
    const [ product, setProduct ] = React.useState(null)

    const fetchProductFromDB = async () => {
        setProduct(await execute({ path: '/product', method: 'GET' }, null, { id: id }))
    }

    React.useEffect(() => { fetchProductFromDB() }, [ id ])

    return Lodash.isNull(product) ? (<div/>) : (
        <div className={classes.wrapper}>
            <Header product={product}/>
            <Content product={product}/>
        </div>
    )

}
// Libraries
import React from 'react'
import Rating from '@material-ui/lab/Rating'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(() => ({
    wrapper: {
        display: 'flex'
    },
    caption: {
        width: 60,
        textAlign: 'right',
        paddingTop: 3,
        paddingRight: 6,
        flexShrink: 0
    },
    value: {
        width: 120,
        flexShrink: 0
    }
}))

export default function Rate({ type, value }) {

    const classes = useStyles()

    return (
        <div className={classes.wrapper}>
            <div className={classes.caption}>{type}</div>
            <div className={classes.rating}>
                <Rating readOnly value={value}/>
            </div>
        </div>
    )

}
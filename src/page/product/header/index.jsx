// Libraries
import React from 'react'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import { green } from '@material-ui/core/colors'

// Components
import Rate from 'Page/product/header/rate'

// Utilities
import { price } from 'Utility/convert'

const useStyles = makeStyles(() => ({
    wrapper: {
        display: 'flex',
        alignItems: 'center'
    },
    name: {
        flexGrow: 1,
        width: '100%',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
    },
    price: {
        flexGrow: 1
    },
    rating: {
        width: 180,
        flexShrink: 0
    },
    caption: {
        fontSize: 22,
        fontWeight: 'bold'
    },
    retail: {
        fontSize: 14,
        paddingLeft: 4,
        textDecoration: 'line-through'
    },
    discounted: {
        fontSize: 22,
        paddingLeft: 4,
        fontWeight: 'bold',
        color: green[700]
    }
}))

export default function Header({ product }) {

    const classes = useStyles()

    return (
        <div className={classes.wrapper}>
            <div className={classes.name}>
                <Typography variant='h5'>{product.name}</Typography>
                <Typography variant='subtitle2'>Brand: {product.brand}</Typography>
            </div>
            <div className={classes.price}>
                <Typography>
                    <span className={classes.caption}>Rs.</span>
                    <span className={classes.retail}>{price(product.retailPrice, 2)}</span>
                    <span className={classes.discounted}>{price(product.discountedPrice, 2)}</span>
                </Typography>
            </div>
            <div className={classes.rating}>
                <Rate type='Product' value={product.productRating}/>
                <Rate type='Overall' value={product.overallRating}/>
            </div>
        </div>
    )

}
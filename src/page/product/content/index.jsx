// Libraries
import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { Carousel } from 'react-responsive-carousel'

const useStyles = makeStyles((theme) => ({
    wrapper: {
        display: 'flex',
        paddingTop: theme.spacing(3)
    },
    images: {
        width: window.innerWidth - 600,
        height: window.innerHeight - 100,
        paddingRight: theme.spacing(3),
        flexShrink: 0
    },
    description: {
        flexGrow: 1
    }
}))

export default function Content({ product }) {

    const classes = useStyles()

    return (
        <div className={classes.wrapper}>
            <div className={classes.images}>
                <Carousel autoPlay={false} width='100%'>
                    {product.images.map((image, index) => {
                        return (
                            <div key={index}>
                                <img src={image}/>
                            </div>
                        )
                    })}
                </Carousel>
            </div>
            <div className={classes.description}>
                <Typography gutterBottom variant='h6'>Product Description:</Typography>
                <Typography>{product.description}</Typography>
            </div>
        </div>
    )

}
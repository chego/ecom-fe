// Libraries
import React from 'react'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import { useHistory } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import { green } from '@material-ui/core/colors'

// Utilities
import { price } from 'Utility/convert'

const useStyles = makeStyles(() => ({
    wrapper: {
        cursor: 'pointer'
    },
    container: {
        padding: 12,
        '&:last-child': {
            paddingBottom: 12
        }
    },
    name: {
        width: '100%',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
    },
    price: {
        textAlign: 'right'
    },
    caption: {
        fontSize: 22,
        fontWeight: 'bold'
    },
    retail: {
        fontSize: 14,
        paddingLeft: 4,
        textDecoration: 'line-through'
    },
    discounted: {
        fontSize: 22,
        paddingLeft: 4,
        fontWeight: 'bold',
        color: green[700]
    }
}))

export default function Product({ product }) {

    const classes = useStyles()
    const history = useHistory()

    return (
        <Card raised className={classes.wrapper} onClick={() => history.push(`/${product.id}`)}>
            <CardMedia component='img' height='250' image={product.image} title={product.name} alt={product.name}/>
            <CardContent className={classes.container}>
                <Typography variant='h5' className={classes.name}>{product.name}</Typography>
                <Typography className={classes.price}>
                    <span className={classes.caption}>Rs.</span>
                    <span className={classes.retail}>{price(product.retailPrice, 2)}</span>
                    <span className={classes.discounted}>{price(product.discountedPrice, 2)}</span>
                </Typography>
            </CardContent>
        </Card>
    )

}
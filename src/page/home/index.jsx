// Libraries
import React from 'react'
import Lodash from 'lodash'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'

// Components
import Header from 'Page/home/header'
import Product from 'Page/home/product'

// Utilities
import { execute } from 'Utility/request'

const useStyles = makeStyles((theme) => ({
    wrapper: {
        paddingTop: theme.spacing(3),
        paddingRight: theme.spacing(3),
        paddingBottom: theme.spacing(3),
        paddingLeft: theme.spacing(6)
    }
}))

export default function Home() {

    const classes = useStyles()
    const [ criteria, setCriteria ] = React.useState({ search: '', order: 'ascending' })
    const [ products, setProducts ] = React.useState([])

    const fetchProductsFromDB = async () => {
        setProducts(Lodash.chunk(await execute({ path: '/products', method: 'GET' }, null, criteria), 3))
    }

    React.useEffect(() => { fetchProductsFromDB() }, [ criteria ])

    return (
        <div className={classes.wrapper}>
            <Header criteria={criteria} onChange={setCriteria}/>
            <Grid container spacing={3}>
                {products.map((row, index) => {
                    return (
                        <Grid container item key={index} xs={12} spacing={3}>
                            {row.map((product) => {
                                return (
                                    <Grid item key={product.id} xs={4}>
                                        <Product product={product}/>
                                    </Grid>
                                )
                            })}
                        </Grid>
                    )
                })}
            </Grid>
        </div>
    )

}
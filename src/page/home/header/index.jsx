// Libraries
import React from 'react'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Input from '@material-ui/core/Input'
import InputAdornment from '@material-ui/core/InputAdornment'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import IconButton from '@material-ui/core/IconButton'
import SearchIcon from '@material-ui/icons/Search'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
    wrapper: {
        display: 'flex',
        paddingBottom: theme.spacing(3)
    },
    padder: {
        flexGrow: 1
    },
    search: {
        width: 200,
        paddingRight: theme.spacing(3),
        flexShrink: 0
    },
    sort: {
        width: 200,
        flexShrink: 0
    }
}))

export default function Header({ criteria, onChange }) {

    const classes = useStyles()
    const [ search, setSearch ] = React.useState(criteria.search)

    const selectOrder = (event) => {
        onChange({ ...criteria, order: event.target.value })
    }

    const updateCriteria = () => {
        onChange({ ...criteria, search: search })
    }
    
    return (
        <div className={classes.wrapper}>
            <div className={classes.padder}/>
            <div className={classes.search}>
                <FormControl>
                    <InputLabel>Search Products</InputLabel>
                    <Input value={search} onChange={(event) => setSearch(event.target.value)} endAdornment={<InputAdornment position='end'>
                            <IconButton onClick={updateCriteria}><SearchIcon/></IconButton>
                        </InputAdornment>}
                    />
                </FormControl>
            </div>
            <div className={classes.sort}>
                <FormControl>
                    <InputLabel>Sorting</InputLabel>
                    <Select fullWidth value={criteria.order} onChange={selectOrder}>
                        <MenuItem value='ascending'>Price: Low to High</MenuItem>
                        <MenuItem value='descending'>Price: High to Low</MenuItem>
                    </Select>
                </FormControl>
            </div>
        </div>
    )

}
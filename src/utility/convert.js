// Libraries
import Lodash from 'lodash'

export const price = (amount, round) => {
    if (Lodash.isUndefined(amount) || Lodash.isNull(amount) || !Lodash.isNumber(amount)) { return '0.00' }
    else {
        amount = Lodash.round(amount, round).toString()
        let dot = Lodash.indexOf(amount, '.')
        let parts = [ Lodash.isEqual(dot, -1) ? '.' : amount.substr(dot) ]
        while (!Lodash.isEqual(parts[0].length, round + 1)) { parts[0] = `${parts[0]}0` }
        if (!Lodash.isEqual(dot, -1)) { amount = amount.substr(0, dot) }
        if (Lodash.isEqual(amount, 0)) { parts.unshift('0') }
        else {
            let interval = [ 3, 2, 2 ], pointer = 0
            while (!Lodash.isEqual(amount.length, 0)) {
                if (amount.length > interval[pointer]) {
                    parts.unshift(amount.substr(amount.length - interval[pointer]))
                    parts.unshift(',')
                    amount = amount.substr(0, amount.length - interval[pointer])
                }
                else {
                    parts.unshift(amount)
                    amount = ''
                }
                pointer = Lodash.isEqual(pointer, 2) ? 0 : (pointer + 1)
            }
        }
        return Lodash.join(parts, '')
    }
}
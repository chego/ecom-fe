// Libraries
import Axios from 'axios'
import Lodash from 'lodash'

// Servers
const Server = 'http://localhost:8000'

const Instance = Axios.create({
    baseURL: Server,
    headers: {
        'content-type': 'application/json'
    }
})

const queryToUrl = (query) => {
    let string = ''
    if (Lodash.isObject(query)) {
        string = '?'
        let keys = Object.keys(query)
        for (let i = 0; i < keys.length; i++) {
            let value = query[keys[i]]
            if (Array.isArray(value)) {
                for (let j = 0; j < value.length; j++) {
                    string = string + keys[i] + '=' + value[j]
                    if (j + 1 < value.length) { string = string + '&' }
                }
            }
            else { string = string + keys[i] + '=' + value }
            if (i + 1 < keys.length) { string = string + '&' }
        }
    }
    return string
}

export const execute = (config, body, query) => {
    return new Promise(async (resolve, reject) => {
        try {
            let options = {
                url: `${Server}${config.path}${queryToUrl(query)}`,
                method: config.method
            }
            if (body) { options.data = body }
            let response = await Instance(options)
            if (Lodash.isEqual(response.status.toString().charAt(0), '2')) { resolve(response.data) }
            else { throw new Error(response.message) }
        }
        catch (error) { reject(error) }
    })
}